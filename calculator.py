# 多态性是指具有不同功能的函数可以使用相同的函数名，这样就可以用一个函数名调用不同内容的函数。
# 在面向对象方法中一般是这样表述多态性：向不同的对象发送同一条消息，不同的对象在接收时会产生不同的行为（即方法）
# 也就是说，每个对象可以用自己的方式去响应共同的消息。所谓消息，就是调用函数，不同的行为就是指不同的实现，即执行不同的函数。
# 加上abstractmethod装饰器后严格控制子类必须实现这个方法
import sys
from abc import ABCMeta, abstractmethod


# 封装同一类：运算
class Operation():
    __metaclass__ = ABCMeta

    def __init__(self):
        self.result = None

    @abstractmethod
    def get_result(self):           # 子类必须实现这个方法
        pass


# 加法运算
class AddOperation(Operation):

    def get_result(self, number_a, number_b):
        self.result = number_a + number_b
        return self.result


# 减法运算
class SubOperation(Operation):

    def get_result(self, number_a, number_b):
        self.result = number_a - number_b
        return self.result


# 乘法运算
class MulOperation(Operation):

    def get_result(self, number_a, number_b):
        self.result = number_a * number_b
        return self.result


# 除法运算
class DivOperation(Operation):

    def get_result(self, number_a, number_b):
        if number_b == 0:
            print("With operator '/', the second number can not be zero.")
            return self.result
        self.result = number_a / number_b
        return self.result


# 实例化
class OperationFactory():

    @classmethod
    def create_operate(self, operator):
        oper = None
        if operator == "+":
            oper = AddOperation()
        elif operator == "-":
            oper = SubOperation()
        elif operator == "*":
            oper = MulOperation()
        elif operator == "/":
            oper = DivOperation()
        else:
            print("Wrong operator.")
        return oper


# 面向对象操作
# 输入<a><运算符><b>
number_a = float(sys.stdin.read(1))
operator = str(sys.stdin.read(1))
number_b = float(sys.stdin.read(1))

oper = OperationFactory.create_operate(operator)
print(oper.get_result(number_a, number_b))
